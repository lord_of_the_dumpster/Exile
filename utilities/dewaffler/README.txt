This is a mod that can be used to fix broken slopes.
It adds a command, "/dewaffle <radius>" that will re-run
natural slopes generation over the area to fix the "waffling"
caused by a long standing bug that sat between naturalslopeslib
and our grass spread ABM.
 To install it, just move it into the mods folder and restart. The
command requires server privs to use.

 I find a radius of about 50 is fairly quick and does a good
chunk of terrain in one go, on Land of Catastrophe's hardware.
