Node and Item Groups used in Exile
==================================

Misc:
-----
not_in_creative_inventory
slippery
falling_node
attached_node
craftedby - Add creator meta data to node; used for crafted by labels and keys
edible - Nodes that are edible - auto adds on_use callbacks to eat
`nobones`: Don't put this item into bones piles. e.g. crafting spots
		Note: This destroys the item upon player death
`unclaimable`: Cannot be nailed down or otherwise owned. e.g. crafting spots
`drug`: indicates whether or not an item or node is a substance of drug-like explicit interest
`timer`: this node runs on a timer -- value is the default timer length

Digging:
---------
Values from 1 - 3, with 1 being hardest
`cracky`: brittle, hard things. e.g. stone
`choppy`: hard, flexible things e.g. wood
`crumbly`: loose things e.g. sand
`snappy`: soft, tearable things e.g. plants.

Air:
--------
`air`: is air or similar; e.g. air_temp nodes and lit nodes from wielded_light

Fire:
-----
`flammable`: can catch fire. Has a 1 in [value] chance of catching fire.
             Use, on_burn = function(pos) for custom post fire results.
`igniter`: can ignite flammable.
`cools_lava`: will cool neighboring lava to basalt
`puts_out_fire`: will extinguish neighboring flames and fires


Liquids:
--------
`water`: 1 = fresh, 2 = salt
`liquid_storage`: indicates whether or not a node (bucket) stores liquid



stone
------
`stone`: naturally occurring hard rock
`soft_stone`: naturally occurring soft sedimentary semi-rock, drops the sediment
`boulder`: stone boulder

Sediment
---------
`sediment`: 1 = loam, 2 = clay, 3 = silt, 4 = sand, 5 = gravel. Things plants might grow on, absorbs water.
`wet_sediment`: same as above, but soaked with water. 1=fresh, 2= salt.
`spreading`: Living surface e.g. clay with grass, spreads to exposed clay.
`fertilizer`: restores depleted agricultural soil on punch
`agricultural_soil`: Gives a boost to plant growth.
`depleted_agricultural_soil`: gives a small boost to plant growth.
`bare_sediment`: defines a sediment that grass could spread to.


Temperature:
-----------
`temp_effect`: 1 = is a source of cooling or heating (node must define `temp_effect` and `temp_effect_max`.
    See climate. Might want to use `climate.air_temp_source`. temp_effect_max is it's own internal temperature. e.g. if a fire wasn't 600C it wouldn't be a fire)
`temp_pass`: 1 = temperature effects can be transmitted through this node (e.g. a toothpick can't shield you against fire.)
`heatable`: Can accumulate heat/cooling from temp_effect sources that use `climate.air_temp_source`.
  Must call `climate.heat_transfer` as part of node timer. Intended for pottery, smelting etc for direct heat transfers from fires etc.
  Value = % of dissipation speed. i.e. 100 means it dissipates at 100% of base speed (like air, which holds heat poorly),
  10 means 10% of base speed (e.g. like metal which holds heat well). Allows different materials to lose heat and different rates.
`temp_flow`: 100 = air_temp nodes can jump through this; like an open door
  Value = % chance of passing through. 100% will always succeed

Plant groups
-------------
`herbaceous_plant`: Soft plant matter.
`woody_plant`: twiggy plant matter. Can make into sticks
`fibrous_plant`: tough stringy plant matter. Anything you might try using for string.
`mushroom`: can grow in dark, likely a mushroom.
`tree`: a tree trunk.
`log`: dropped on felling tree. Used for timber crafts
`cane_plant`: cane like plant, grows on top of itself in a pillar
`seed`: a plant seed or spore. Will grow into a seedling.
`seedling`: unused?
`flora`: wild plants, will spread
`bioluminescent`: a living thing that glows.
`woodslab`: a log that's been cut in half, e.g. stairs/slabs


Animals groups
--------------
`class`: 1 for "land-borne", 2 for "sea-borne", 3 for "amphibious", 4 for "flying" (not functional)
`burnt`: false or true (true means it will try to find items with a "burned" or "burnt" suffix), should not be set manually

Misc Animals groups/data
--------------
`carcass`: animal carcasses; 1-3 is "invert" (invertebrate), 4-6 is "bird", 7-9 is "fish". +1 is medium, +2 is large
`egg`: animal eggs; 1 for "invert" (invertebrate), 2 for "bird", 3 for "fish"
`raw-cooked`: for animal carcasses; 1 for raw, 2 for cooked, 3 for burnt


pottery groups
--------------
`pottery`: can be broken into broken pottery


construction material groups
--------------
`masonry`: solid bricks, blocks etc (e.g. can collect puddles)
`ladder`: A solid ladder that needs support underneath

Tool groups:
--------------
`club`: 1= blunt weapon. can stun catch mobs on right click
