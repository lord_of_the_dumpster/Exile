# Changelog:
## [2024-07-18][2023-07-18__0.3.18]
   - Added glass doors and trapdoors
   - Hang an oil lamp from the ceiling
   - Support for the Unicode Signs [ucsigns] mod
   - Better texture for roof/floor tiles
   - Inventory images for coarse & fine fabrics

## [2024-06-04][2023-06-04__0.3.17f]
   - Potash liquid is caustic

## [2024-04-22][2023-04-22__0.3.17e]
## [2024-04-06][2023-04-06__0.3.17d]
   - bugfixes

## [2024-03-28][2023-03-28__0.3.17c]
   - Support for ExchangeClone mod
   - more bugfixes

## [2024-03-24][2023-03-23__0.3.17b]
## [2024-03-23][2023-03-23__0.3.17a]
   - bugfixes

## [2024-03-22][2023-03-22__0.3.17]
   - Informational states displayed on character tab
   - Drystack and Rammed earth arches: build a wall, knock out the middle
   - server command: /recover_inv <player> to restore inv lost on restart

## [2024-03-03][2023-03-03__0.3.16]
  - Replaced NC-licensed Gundu, Exile should be fully free now
  - WITT for Exile: show the names of things when you press the zoom key
  - Improved light meter
  - Added /crouch as alternative to double-tapping shift, in case of lag
  - Less trouble with lowering ropes/ladders

## [2024-01-16][2023-01-16__0.3.15d]
  - Add a player model to clothing page, too!
  - Increase /ping timeout on servers
  - Fix for a mobile issue with bed rest

## [2024-01-09][2023-01-09__0.3.15c]
  - Fixed a crafting screen bug
  - Fixed some positional audio quirks

## [2024-01-09][2023-01-09__0.3.15b]
  - Added a player model to the character tab
  - Fix for the "disappearing hammer" bug

## [2024-01-08][2023-01-08__0.3.15]
  - Bones of previous exiles, with various items
  - Sun hat and sandals
  - Fires more likely to die out than burn forever
  - SkinsDB support for folks who want that
  - /ping command to reveal your location to players out of range for nametags
  - Comfort and rest improvements
  - UI improvements
  - Fixed hair colors
  - /restart will fix bugged players who closed the death formspec

## [2023-12-01][2023-12-01__0.3.14b]
  - Added tile floor block to match roof, with stairs and slabs
  - Added support for Visual Harm 1ndicators mod

## [2023-11-11][2023-11-11__0.3.14]
  - Transporters now send all objects that are on the pad
  - Improved letter and bones with info on player/lives
  - Sound for pots
  - More small bugs fixed

## [2023-10-13][2023-10-13__0.3.13.37]
  - Commit #1337
  - Bugfixes for v0.3.11

## [2023-09-30][2023-09-30__0.3.11]
  - Improved animal behavior
  - Increase portion size for meat
  - Quickslot numbers on the HUD
  - Enforced range limits for transporters
  - Add a stone etching tool
  - New environmental sounds
  - Nicer glass sounds
  - Spear improvements
  - Clearer glass windows
  - Added startexile.sh server script with optional /version command

## [2023-05-25][2023-05-25__0.3.10] — 0.3.10
  - Added a watering can, to turn dry soil wet
  - Ability to crouch/crawl by double-tapping shift
  - Can now dump a cooking pot's contents out
  - Can also empty unused dye from a dye pot
  - Antiquorium ladders are solid now
  - Spears are craftable again
  - Minor fog improvements

## [2023-04-07][2023-04-07__0.3.9e] — 0.3.9e
  - Add theme song "Earth and Stone" by Kevin Hartnell
  - Wattle connects to drystack
  - Keep leftover plant bundles that are not dyes, to see what you've tried
  - Added a dewaffler utility to fix broken terrain caused by the prior bug
  - Fix for opaque white fog banks
  - Usual round of bugfixes

## [2022-12-31][2022-12-31__0.3.9d] — 0.3.9d
  - Add an alignment tool to complement the lever
  - Fix grass spreading, grass dying in shade, and landscape waffling
  - Gateway effects for incoming characters, courtesy of Dokimi
  - Better intro screen
  - Bugfixes

## [2022-12-076][2022-12-07__0.3.9c] — 0.3.9c
  - Misc bugfixes

## [2022-11-26][2022-11-26__0.3.9b] — 0.3.9b
  - Volcanic ash on volcanoes
  - Bugfixes

## [2022-11-04][2022-11-04__0.3.9] — 0.3.9
  - New HUD design
  - Iron lantern for mid-late game players
  - iron nails to add protection to nodes
  - bed can now store a blanket
  - cobbles, small loose stones to complement boulders
  - ability to combine small wood fires after burning
  - animal probe can get player stats
  - options for double-click to eat: /eat2x, and wide hud: /hud16 
  - "crafted by" labels for craft stations
  - Folded Minetest 5.3.0 compatibility into mainline
  - More bugfixes than you can shake a digging stick at

## [2022-08-05][2022-08-05__0.3.8d] — 0.3.8d and 0.2.11d
  - Minor bugfixes

## [2022-08-05][2022-08-05__0.3.8c] — 0.3.8c and 0.2.11c
  - enabled shadows when running on MT 5.6.0+
  - Pots fill with rainwater even if the player is away
  - Many, many bug fixes

## [2022-07-08][2022-07-08__0.3.8b] — 0.3.8b and 0.2.11b
  - Cheaper rammed earth
  - Improved Sarkamos life cycle and behavior
  - Cooking pot improvements
  - Fixed wooden & iron chests' trashcan icon, dig & replace to reload
  - Misc bugfixes

## [2022-06-25][2022-06-25__0.3.8a] — 0.3.8a and 0.2.11a
  - Reverted betterfall mod due to problems on some systems

## [2022-06-24][2022-06-24__0.3.8] — 0.3.8 and 0.2.11
  - New tech rebalance, multiple ways up the tech tree
  - Massive rings left scattered on the surface by the Ancients
  - Throwable spears for hunting and fighting
  - Improved canoe and airboat performance; the airboat strafes with AUX1
  - Fire resistance for tree trunks and other nodes
  - Ability to cut sedimentary rock blocks intact with a chisel
  - Clump falling for drystack/thatch, build ceilings with supports
  - Better, more survivable gundu fish, enables fish farming
  - Can eat unmelted snow for thirst, with drawbacks
  - Spanish translation


## [2022-05-28][2022-05-28__0.3.7] — 0.3.7 and 0.2.10
  - balance: limited fire spread for multiplayer
  - many airboat improvements, including strafe movement with AUX1
  - Haze overlay warns of extreme heat
  - animal probes to watch the health and age of livestock
  - underwater plants spread and replenish
  - beds and thatch reduce fall damage
  - some valuable nodes have protection in multiplayer
  - beginnings of i18n French translation, early translation support

## [2022-04-16][2022-04-16__0.3.6] — 0.3.6 and 0.2.9
 - Craftable dyes, made from randomly selected plants
 - Wielded_light replaces Illumination mod
 - Iron chests and basic protection for valuable items
 - Trees regrow after a rainy season, not only during rain
 - Transfer water directly between pots and bottles
 - Further cooking pot improvements

## [2022-02-25][2022-02-25__0.3.5b] — 0.3.5b and 0.2.8b
 - flowing water no longer supports player weight in MT 5.5.0
 - /suicide command for players who are stuck
 - added frost overlay to the HUD when you're dangerously cold
 - cooking pot now available, still somewhat WIP
 - many minor bugfixes and improvements

## [2022-01-21][2022-01-21__0.3.5] — 0.3.5 and 0.2.8
  - Blankets to keep warm.
  - Foods can now burn if left cooking too long.
  - Mobs should no longer `stun`-lock when hit repeatedly.
  - Nerf: Increased fall damage.
  - You can use a `Stick` to bar a door, to prevent entry.
  - New animations, thanks to ***MisterE***.
  - Track how many incarnations you’ve had in the `Character` tab.
  - `Glow Worm`s in caves.
  - Nerf: `Stick`s don’t connect with right-angle corners anymore.
  - Nerf: `Mudbrick` turns to clay when broken, in the same manner as bricks lose mortar.
  - Mapgen: `carpathian` is now supported, for a harder game world.
  - No more beach spiders.
  - New food system infrastructure.
  - Experimental biomes, enable under settings to test.
  - Miscellaneous bug-fixes and quality of life improvements.

## [2021-11-01][2021-11-01__0.3.4] — 0.3.4 and 0.2.7
  - New player API: Head tracking, multiplayer skins, male and female characters.
  - Added an introduction screen for new players, and a chat display of the day/season/year with in-universe style.
  - Place snow/ice/water on top of a clay `Water Pot`, and melting will fill it.
  - Buff: Food value of meat.
  - Improved `Torch` stacking by rounding-off burn times.
  - New chat commands: `/set_tempscale`, `/date`.
  - Better indoors check on `Minetest 5.4.x`.

## [2021-09-25][2021-09-25__0.3.3] — 0.3.3 and 0.2.6
  - Plants now grow even when players are away from the area.
  - Fireproof: `Iron Doors` and `Iron Trapdoors`.
  - Ability to mix `Wet Loam` and agricultural soil.
  - Renamed `Stone Chopper` to `Stone Knife` for clarity.
  - More bug fixes and improvements.

## [2021-09-12][2021-09-12__0.3.2] — 0.3.2 and 0.2.5
  - Glassworking! Make windows, and make glass bottles that can hold water, and which stack.
  - Agricultural soils erode down to a soil slab instead of vanishing.
  - Fixed a baking crash.

## [2021-07-18][2021-07-18__0.3.1] — 0.3.1 and 0.2.4
  - `Oil Lamp`s can be switched off with right-click, and lit with `Fire Stick`s.
  - `Oil Lamp`s can hold more oil and now burn oil slower.
  - Various bug-fixes.

## [2021-06-12][2021-06-12__0.3.0] — 0.3.0 and 0.2.3
  - Initial release for `Minetest 5.4.0`, use Exile 0.2 series for 5.3.0 version.
  - New dependency: `naturalslopeslib` provides better hillsides.
  - Ability to throw `Torch`es for light.
  - Settings for `breaktaker`.
  - Ability to display temperature in Celsius, Fahrenheit, or Kelvin. (Using global settings.)
  - Food burns instead of vanishing, thanks to ***ts***.
  - Farming improvements.
  - Fixes for fire and rain.
  - Many fixes for crashes and bugs.

## [2021-02-15][2021-02-15_Mantar] — Now maintained by Mantar

## [2020-12-29][2020-12-29__0.2.2] — 0.2.2
  - New HUD.
  - Character tab.
  - Health effects.
  - Drugs: `Tiku`, `Tang`, `Metastim`.
  - Added `megamorph` surface air-shafts.
  - Fixed clothing infinite tolerance bug.
  - Added `Iron Fittings` for carpentry crafting.
  - Young vs. old `Tangkal` trees.
  - Added `Tiken` woody cane.
  - Cave sediments and mushrooms.
  - Defined `puts_out_fire` group (e.g. mud now puts out fire), and `masonry` group.
  - Created a walkthrough document for players who want one.
  - Added `Loose Wattle`.
  - Added `Mashed Anperla`.
  - Miscellaneous minor fixes and balancing.

## [2020-09-25][2020-09-25__0.2.1] — 0.2.1
  - Fixed exile-letter crash.
  - Fixed saltwater surface sediment crash.
  - Fixed multiplayer health-tab crash.
  - Animals balanced and bug-fixed.
  - Adjusted ice freezing and thawing.

## [2020-08-02][2020-08-02__0.2.0] — 0.2.0
  - Clothing (plus multi-skin model, new player API), `Weaving Frame`, and temperature tolerance via clothing.
  - Lore: exile letter.
  - Added `Sleeping Spot`.
  - New mobs: `Darkasthaan`, `Impethu`, `Pegasun`, `Sneachan`.
  - Cookable animal carcasses.
  - Graffiti: glow paint.
  - New building materials: `Rammed Earth`, bricks and mortar, roof tiles, wooden doors/floors/stair, mortared masonry.
  - Fixed dungeon loot.
  - Added more artifacts. (`Airboat`, art, transporter, `Wayfinder`,
  ….)
  - Added three artifact species of mushrooms for sustaining fibre, food/water, and sticks, absent sunlight.
  - Split Geomoria mod's biomes into realms (highways, and city structures).
  - Added more `geomorphs`.
  - `Gneiss` and `Jade` for deep-underground biome.
  - Better-looking `Bones`.
  - Newly-formatted colouring for skies.
  - Chat commands for controlling climate variables.
  - `Design Guide`.
  - Updated menu-screen backgrounds.
  - Updated version of crafting mod.
  - Various minor fixes.
  - Various minor balance changes.
  - Various minor additions.
  - `Minetest 5.3.0` compatibility.

## [2020-03-11][2020-03-11__0.1.0] — 0.1.0
  - Initial release.

## [2020-03-10][2020-03-10_Dokimi] — Founded by Dokimi

[2022-01-21__0.3.5]: (https://github.com/DokimiCU/Exile/commits/5d3dfb4fc9e8636d57a00cb59df3b6c36edf4d9c)
 <!-- Unix time 1642873579 -->
 <!-- Fri Jan 21 16:46:19 2022 UTC-0800 -->
 <!-- 2022-01-22 00:46:19      UTC+0000 -->
 <!-- (https://content.minetest.net/packages/Mantar/exile/releases/10746/download/) -->

[2021-11-01__0.3.4]: (https://github.com/DokimiCU/Exile/commits/0889b4abd431fd578ea76a5fcc3a70aace7dfcb9)
 <!-- Unix time 1635744365 -->
 <!-- Sun Oct 31 18:26:05 2021 UTC-0700 -->
 <!-- 2021-11-01 01:26:05      UTC+0000 -->
 <!-- (https://content.minetest.net/packages/Mantar/exile/releases/9596/download/) -->

[2021-09-25__0.3.3]: (https://github.com/DokimiCU/Exile/commits/f5fc39c8b8ce7f6d2dec23caecbd6a59977d0520)
 <!-- Unix time 1632604522 -->
 <!-- Sat Sep 25 14:15:22 2021 UTC-0700 -->
 <!-- 2021-09-25 21:15:22      UTC+0000 -->
 <!-- This was the initial release of v0.3.3 but is now deprecated in favor of v0.3.3g -->

[2021-09-12__0.3.2]: (https://github.com/DokimiCU/Exile/commits/350ed936b3370fae204cd2c31e0491525dc7928d)
 <!-- Unix time 1631484548 -->
 <!-- Sun Sep 12 15:09:08 2021 UTC-0700 -->
 <!-- 2021-09-12 22:09:08      UTC+0000 -->
 <!-- (https://content.minetest.net/packages/Mantar/exile/releases/9282/download/) -->

[2021-07-18__0.3.1]: (https://github.com/DokimiCU/Exile/commits/d96a3aed6d292b216ec6ed470eece6ff5d2468c6)
 <!-- Unix time 1625860964 -->
 <!-- Fri Jul  9 13:02:44 2021 UTC-0700 -->
 <!-- 2021-07-09 20:02:44      UTC+0000 -->
 <!-- (https://content.minetest.net/packages/Mantar/exile/releases/8536/download/) -->

[2021-06-12__0.3.0]: (https://github.com/DokimiCU/Exile/commits/9939187661e50cf06874b8715027ae7b5c4eca8f)
 <!-- Unix time 1623520818 -->
 <!-- Sat Jun 12 11:00:18 2021 UTC-0700 -->
 <!-- 2021-06-12 18:00:18      UTC+0000 -->
 <!-- (https://content.minetest.net/packages/Mantar/exile/releases/8046/download/) -->

[2021-02-15_Mantar]: (https://github.com/DokimiCU/Exile/commits/69101fca2bfc9698ade75342b90d78940b7c8f33)
 <!-- Unix time 1613421013 -->
 <!-- Mon Feb 15 12:30:13 2021 UTC-0800 -->
 <!-- 2021-02-15 20:30:13      UTC+0000 -->
 <!-- This was the first commit on GitHub from the new maintainer, Mantar. -->

[2020-12-29__0.2.2]: (https://github.com/DokimiCU/Exile/commits/dc1e210edc012f41d0cc6fdfeab93aeff9e5e7cd)
 <!-- Unix time 1609284479 -->
 <!-- Wed Dec 30 12:27:59 2020 UTC+1300 -->
 <!-- 2020-12-29 23:27:59      UTC+0000 -->
 <!-- (https://github.com/DokimiCU/Exile/releases/tag/v0.2.2) -->

[2020-09-25__0.2.1]: (https://github.com/DokimiCU/Exile/commits/d2c3c557aaf687aae2cabfd1e17117bc9b1dd681)
 <!-- Unix time 1601009177 -->
 <!-- Fri Sep 25 16:46:17 2020 UTC+1200 -->
 <!-- 2020-09-25 04:46:17      UTC+0000 -->
 <!-- (https://github.com/DokimiCU/Exile/releases/tag/v0.2.1) -->

[2020-08-02__0.2.0]: (https://github.com/DokimiCU/Exile/commits/32bad7e2610af17b4155b60cb656bd3695601ae3)
 <!-- Unix time 1596361238 -->
 <!-- Sun Aug  2 21:40:38 2020 UTC+1200 -->
 <!-- 2020-08-02 09:40:38      UTC+0000 -->
 <!-- (https://github.com/DokimiCU/Exile/releases/tag/v0.2) -->

[2020-03-11__0.1.0]: (https://github.com/DokimiCU/Exile/commits/06262e7e2eb6e3e974562bc5547f10b2eab9a71c)
 <!-- Unix time 1583888273 -->
 <!-- Wed Mar 11 13:57:53 2020 UTC+1300 -->
 <!-- 2020-03-11 00:57:53      UTC+0000 -->
 <!-- It appears Dokimi did not tag the initial release. -->

[2020-03-10_Dokimi]: (https://github.com/DokimiCU/Exile/commits/7f23572163274f9a9d3417e92c3efae31dddd00f)
 <!-- Unix time 1583885403 -->
 <!-- Wed Mar 11 13:10:03 2020 UTC+1300 -->
 <!-- 2020-03-11 00:10:03      UTC+0000 -->
 <!-- This was the first commit on GitHub.
	Presumably the code was developed for more than at least one day.
	The date Dokimi began working on the code does not appear to have been publicized. -->
