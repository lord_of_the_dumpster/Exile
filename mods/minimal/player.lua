local clear_ping_delay = tonumber(minetest.settings:get(
				     "exile_clear_ping_delay")) or 20
local waypoints = {}

if minetest.settings:get_bool("unlimited_player_transfer_distance", true) then
   return -- don't need a ping command if everyone can be seen anyway
end

local function add_waypoint(name, viewername, viewer, pos)
   local id = viewer:hud_add({
	 hud_elem_type = "waypoint",
	 number = 0xFFFFFF,
	 name = name,
	 text = "m",
	 world_pos = pos
   })
   if not waypoints[name] then -- initialize
      waypoints[name] = { }
   end
   waypoints[name][viewername] = { handle = id , obj = viewer }
end

local function clear_waypoint(name)
   if not waypoints[name] then return end
   for _, data in pairs(waypoints[name]) do
      if minetest.is_player(data.obj) and data.handle then
	 data.obj:hud_remove(data.handle)
      end
   end
   waypoints[name] = {}
end

local timestamp = {}

minetest.register_chatcommand("ping",{
	privs = "shout",
	func = function(myname,param)
	   local nowtime = minetest.get_gametime()
	   if timestamp[myname] and ( timestamp[myname] +20 ) > nowtime then
	      return false, "You can't use this command "..
		 " more than once per 20 seconds."
	   end
	   local pos = minetest.get_player_by_name(myname):get_pos()
	   local targets
	   local isplayer = minetest.get_player_by_name(param)
	   if param and param ~= "" and not isplayer then
	      return false, "Player "..param.." not found!"
	   end
	   if isplayer then -- single target
	      targets = { [param] = minetest.get_player_by_name(param) }
	   end
	   for _, player in pairs(targets or
				  minetest.get_connected_players()) do
	      local theirname = player:get_player_name()
	      if myname ~= theirname then
		 add_waypoint(myname, theirname, player, pos)
		 timestamp[myname] = nowtime
	      end
	   end
	   minetest.after(clear_ping_delay, clear_waypoint, myname)
	end
})
